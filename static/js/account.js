function init(id) {
    var id = id || document.location.href.split('/')[document.location.href.split('/').length - 2];
    ajaxRequest(config.DOCUMENT_ROOT + 'api/users/' + id, null, function(data) {
        var profile = '';
        var user_hierarchy = '';
        var user_status = '';

        if(data.image){
            document.querySelector(".user-photo img").src= data.image
        }

        for (var prop in data.fields) {
            if (!data.fields.hasOwnProperty(prop)) continue

            if (data.fields[prop]['type'] == 's') {
                profile += '<li><p>' + prop + '*</p><span>' + data.fields[prop]['value'] + '</span></li>';
            }

            if (data.fields[prop]['type'] == 'h') {
                user_hierarchy += '<li><p>' + prop + '*</p><span>' + data.fields[prop]['value'] + '</span></li>';

            }
            if (data.fields[prop]['type'] == 'b') {
                var is_checked = data.fields[prop]['value'] ? 'checked' : '';
                user_status += '<div class="field"><input type="checkbox" name="check1" id="check1" ' + is_checked + '><label>' + prop + '<span></span></label> </div>';
            }
            if (data.fields[prop]['type'] == 't') {
                profile += '<li><p>' + prop + '*</p><textarea rows = "3" disabled>' + data.fields[prop]['value'] + '</textarea>';
            }
        }

        var division_list  = data.division_fields;
        var list = Object.keys(division_list);
        var divisions = ''
        if(list.length){
            divisions = '<li>Привязка к департаментам:</li>'
        }
        else{
            divisions = '<li>У пользователя нету привязки к департаментам</li>'
        }
        for(var prop in division_list){
            divisions += '<li><span>' + prop + '</span></li>';
        }

        document.getElementById('user-info').innerHTML = profile;
        document.getElementById('status').innerHTML = user_hierarchy;
        document.getElementsByClassName('user-status')[0].innerHTML = user_status;

        document.getElementById('status_division').innerHTML = divisions;
    })
}

function partners_initialize() {

    var id = id || document.location.href.split('/')[document.location.href.split('/').length - 2];


    document.getElementById('partner_info').innerHTML = '';
     document.getElementById('partner_table').innerHTML = '';
    //partner_table
    check_partner_info(id);
}
//Проверка создания партнерки на пользователе
function check_partner_info(id,page) {
  var page = page || 1;
  var url = config.DOCUMENT_ROOT + '/api/partnerships/?user=' + id + '&page=' + page;

    $.ajax({
        type: "GET",
        url: url,
        data: null,
        success: function(data) {

            if (data.results.length) {
                partner_info(data);
                document.getElementById('deals').style.display = 'block';
                document.getElementById('update_partnership_wrap').style.display = 'block'
                document.getElementById('create_partnership_wrap').style.display = 'none'
            } else {
                document.getElementById('create_partnership_wrap').style.display = 'block';

                document.getElementById('deals').style.display = 'none';
                document.getElementById('update_partnership_wrap').style.display = 'none'

            }
            create_drobbox_father('dropbox_wrap');
        }


    });
}

//Пагинация
function partner_info(data) {

    var data = data.results[0]
    var fields = data.fields;
    var deal_fields = data.deal_fields;
    var id = data.id;
    var html = ''

    for (var i in fields) {
        html += '<p>' + i + ': <span>' + fields[i].value + '</span></p>'
    }
    document.getElementById('partner_info').innerHTML = html;
    document.getElementById('show_update_partnership').setAttribute('id_partnership', id);
  
    var html = '<table><tr><td>ID</td><td>Клиент</td><td>Ответственный</td><td>Дата</td><td>Сума</td><td>Описание</td><td>Статус</td></tr>';

    if (!deal_fields || deal_fields.length == 0) {
        document.getElementById('partner_table').innerHTML = '' //'Нету deal_fields';
        return ''
    }

    for (var i = 0; i < deal_fields.length; i++) {

        html += '<tr>'
        for (var p in deal_fields[i]) {

            if (deal_fields[i][p].verbose == 'id') {
                var id = deal_fields[i][p].value;
                html += '<td data-id="' + id + '">' + deal_fields[i][p].value + '</td>'
                continue
            }

            html += '<td>' + deal_fields[i][p].value + '</td>'
        }
        html += '</tr>'
    }


    html += '</table>'
    document.getElementById('partner_table').innerHTML = html;

    //Обновление сделки
    $("#partner_table tr").click(function() {

        var id = $(this).find("td[data-id]").attr('data-id');

        if (id) {
            showUpdateDeal(id)
        }
    })

}

function create_drobbox_father(id) {

    //id - parant wrapper element select 
    ajaxRequest(config.DOCUMENT_ROOT + 'api/partnerships/?is_responsible=' + 2, null, function(data) {
        var data = data.results;
        var html = '<div class="sandwich-wrap partner_drop "><span class="sandwich-cont">Отвественный</span>' +
            '<span class="sandwich-button"></span><div class="sandwich-block"><ul>';
        for (var i = 0; i < data.length; i++) {
            html += '<li data-id="' + data[i].id + '"><span>' + data[i].fullname + '</span></li>';
        }
        html += '</ul></div></div>';
        document.getElementById(id).innerHTML = html;

    });

}


function create_partnerships(data) {



    var json = JSON.stringify(data);
    ajaxRequest(config.DOCUMENT_ROOT + 'api/create_partnership', json, function(JSONobj) {

        if (JSONobj.status) {
            showPopup(JSONobj.message);
            partners_initialize();
        } else {

            showPopup(JSONobj.message);
        }
    }, 'POST', true, {
        'Content-Type': 'application/json'
    });

}


function update_partnership(data) {


    var json = JSON.stringify(data);
    ajaxRequest(config.DOCUMENT_ROOT + 'api/update_partnership', json, function(JSONobj) {

        if (JSONobj.status) {
            showPopup(JSONobj.message);
            partners_initialize();
            removePopups('update_partnership_popup')
        } else {
            removePopups('update_partnership_popup')
            showPopup(JSONobj.message);
        }
    }, 'POST', true, {
        'Content-Type': 'application/json'
    });

}

function delete_partnership(id){




    var data = {
    "id": id,
}

    var json = JSON.stringify(data);
    ajaxRequest(config.DOCUMENT_ROOT + 'api/delete_partnership', json, function(JSONobj) {

        if (JSONobj.status) {
            showPopup(JSONobj.message);
            partners_initialize();
            //removePopups('update_partnership_popup')
        } else {
            //removePopups('update_partnership_popup')
            showPopup(JSONobj.message);
        }
    }, 'POST', true, {
        'Content-Type': 'application/json'
    });


}
function showUpdatePartnerShip(id) {

    var el_id = 'update_partnership_popup';


    var html = '<p> Обновление партнерки : </p>' +
        '<input type="text" class="datepicker" id="datepicker_update" value="" style="cursor:pointer;">' +
        '<div id="dropbox_wrap_popup"></div>' +
        '<p> Сума <input type="text" value = "0" name= "money" id= "money_update" > </p>' +
        'Консультат (Да/Нет) <input type="checkbox" id="is_consultant_update" style="display:inline-block; width: 30px" >'+
        '<input type="button"  value = "обновить партнерку" id= "update_partnership" >'

    removePopups(el_id)
    createPopups(el_id, html, function() {});


    create_drobbox_father('dropbox_wrap_popup');




    showPopupById(el_id);

    $("#datepicker_update").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(data, t) {

        }
    }).datepicker("setDate", new Date());


    document.getElementById('update_partnership').addEventListener('click', function() {

        var data = {
            'id': id
        }

        var parent_id = document.getElementById('dropbox_wrap_popup').getElementsByClassName('sandwich-cont')[0].getAttribute('data-id');

        if (parent_id) {
            data.responsible = parent_id;
        }
        if( document.getElementById('is_consultant_update').checked ){

        }

        var money = parseInt(document.getElementById('money_update').value);
        if (money) {
            data.value = money
        }
        var date = document.getElementById('datepicker_update').value
        if (date) {
            data.date = date
        }

       if (document.getElementById('is_consultant_update').checked) {
            data['is_responsible'] = true
        } else {
            data['is_responsible'] = false
        }
        update_partnership(data)
    })


    // update_partnership(id,responsible,value)
}

function showUpdateDeal(id) {

    var el_id = 'update_deal';


    var html =

        '<p>  Описание сделки: </p>' +
        '<textarea  id="description_update" placeholder="Описание сделки" cols = "30" rows="10"></textarea>' +
        '<p> Cумма сделки  </p><input type="text"  id="deals_value_update" placeholder="сумма сделки">' +
        '<p> Cтатус  завершенности   <input type="checkbox" id="is_responsible" style="display:inline-block; width: 30px" ></p>' +
        '<input type="button" value="Обновить сделку" id="update_deals"   data-id = "' + id + '">'

    removePopups(el_id)
    createPopups(el_id, html, function() {});
    showPopupById(el_id);

    $("#datepicker_deals_update").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(data, t) {
            //  console.log(  data );
            // ajaxRequest(path, null, createEventsLink)
        }
    }).datepicker("setDate", new Date());

    document.getElementById('update_deals').addEventListener('click', function() {
        var deal_id = this.getAttribute('data-id');
        if (!deal_id) {
            return
        }

        var money = parseInt(document.getElementById('deals_value_update').value);

        var description = document.getElementById('description_update').value
        var data = {
            id: deal_id /*,date: date*/
        }
        if (money) {
            data.value = money
        }
        if (description) {
            data.description = description
        }


        if (document.getElementById('is_responsible').checked) {
            data['done'] = true
        }
        update_deals(data)
    })
}


function showCreateDeal() {

    var el_id = 'create_deal';
    

    var html = '<p>Cоздание сделки</p>' +
        'Дата сделки <input type="text" class="datepicker" id="datepicker_deals" value="" style="cursor:pointer;" placeholder="Дата сделки">' +

        '<p>  Описание сделки: </p><textarea  id="description" placeholder="Описание сделки" cols = "30" rows="10"></textarea>' +
        '<p> Cтатус  завершенности   <input type="checkbox" id="is_responsible" style="display:inline-block;width: 30px" ></p>' +
        '<input type="button" value="Создать сделку" id="create_deals">'

    removePopups(el_id)
    createPopups(el_id, html, function() {});
    showPopupById(el_id);


    $("#datepicker_deals").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(data, t) {

        }
    }).datepicker("setDate", new Date());

    document.getElementById('create_deals').addEventListener('click', function() {
        var xhr = new XMLHttpRequest();
        //       var money = parseInt(document.getElementById('deals_value').value);
        //  console.log(money)
        var date = document.getElementById('datepicker_deals').value || new Date();
        var description = document.getElementById('description').value
            //Треба передавать айді не зареєстр користувача а id анкети

        var id = document.location.href.split('/')[document.location.href.split('/').length - 2];


        /*
                if(!money){
                    showPopup('Введите сумму');
                    return
                }
        */
        if (!description) {
            showPopup('Введите описание');
            return
        }
        var data = {
            "date": date,
            "user": id,
            "description": description,

        }

        if (document.getElementById('is_responsible').checked) {
            data['done'] = true
        } else {
            data['done'] = false
        }

        var json = JSON.stringify(data);
        ajaxRequest(config.DOCUMENT_ROOT + 'api/create_deal', json, function(JSONobj) {
          //  console.log(JSONobj)
            if (JSONobj.status) {
                showPopup(JSONobj.message);
                // document.getElementById('deals_value').value = '';
                // document.getElementById('description').value = '' ;
                partners_initialize();
                removePopups(el_id)
            } else {
                removePopups(el_id)
                showPopup(JSONobj.message);
            }
        }, 'POST', true, {
            'Content-Type': 'application/json'
        });



    })



}



function update_deals(data) {
    var xhr = new XMLHttpRequest();
    /*
var data = {
    "id": id,
    "date": date,
    "value": value, 
    "description": description

}
*/
    var json = JSON.stringify(data);
    xhr.open("POST", config.DOCUMENT_ROOT + '/api/update_deal', true)
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                partners_initialize();
                hidePopupById('update_deal')
            }
        }
    }
    xhr.send(json);
}


$(function() {

    init();
    partners_initialize();
    summit_info(document.location.href.split('/')[document.location.href.split('/').length - 2]);



    $("#datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(data, t) {

        }
    }).datepicker("setDate", new Date());




    $("#datepicker_update").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(data, t) {

        }
    }).datepicker("setDate", new Date());


    //Редактировать профиль
    document.getElementsByClassName('edit_user_id')[0].addEventListener('click', function(e) {
        e.preventDefault();
        var id = document.location.href.split('/')[document.location.href.split('/').length - 2];
        document.location.href = '/account_edit/' + id;
    });

    //Отправить пароль
    document.getElementsByClassName('send_pass')[0].addEventListener('click', function() {
        var data = {
            "id": document.location.href.split('/')[document.location.href.split('/').length - 2]
        }
        var json = JSON.stringify(data);
        ajaxRequest(config.DOCUMENT_ROOT+'api/send_password/', json, function(data) {
            showPopup(data.message)
        }, 'POST', true, {
            'Content-Type': 'application/json'
        });
    });


    document.getElementById('create_partnership').addEventListener('click', function() {
      
        var money = parseInt(document.getElementById('money').value);
        var date = document.getElementById('datepicker').value || new Date();
        
        var id = id || document.location.href.split('/')[document.location.href.split('/').length - 2];
        var data ={'date':date,'user' : id}
        if (!money) {
            showPopup('Введите сумму');
            return
        }
        data.value = money
        var parent_id = document.getElementsByClassName('father')[0].getElementsByClassName('sandwich-cont')[0].getAttribute('data-id');

        if (!parent_id) {
            showPopup('Выберите отвественного ');
            return
        }
        data.responsible = parent_id

        
        if (document.getElementById('is_consultant').checked) {
            data['is_responsible'] = true
        } else {
            data['is_responsible'] = false
        }
       

        create_partnerships(data)
    });



    document.getElementById('show_update_partnership').addEventListener('click', function() {

        var id = this.getAttribute('id_partnership');
        if (!id) {
            return
        }

        showUpdatePartnerShip(id);
            
    })


        document.getElementById('delete_partnership').addEventListener('click', function() {

        var id = document.location.href.split('/')[document.location.href.split('/').length - 2];
        if (!id) {
            return
        }

        delete_partnership(id);
            
    })


   document.getElementById('summit-register').addEventListener('click',function(){
        registerToSummit();
    })


function registerToSummit(){

    var el_id = 'register_summit_popup';


               var html = '<p> Регистрация на саммит : </p>'+
                             '<div id="summit_wrap_popup"></div>'+
                             '<p> Сума <input type="text" value = "" name= "money" id= "summit_money" > </p>'+
                             '<input type="button"  value = "регистрация на саммит" id= "register_summit" >'

            removePopups(el_id)
            createPopups(el_id, html, function() {});

            create_drobbox_summit('summit_wrap_popup');

            showPopupById(el_id);

        document.getElementById('register_summit').addEventListener('click',function(){

        var summit_id = document.getElementById('summit_wrap_popup').getElementsByClassName('sandwich-cont')[0].getAttribute('data-id'),
            id = document.location.href.split('/')[document.location.href.split('/').length - 2],
            money = document.getElementById('summit_money').value;
        update_summit(id,summit_id,money)
      })
  }

function create_drobbox_summit(id){

        var user = document.location.href.split('/')[document.location.href.split('/').length - 2];
        ajaxRequest(config.DOCUMENT_ROOT + 'api/summit/user/?id=' + user, null, function(data) {
            /*var data = data.results;*/
             console.log(data);
            var html = '<div class="sandwich-wrap drop_summit"><span class="sandwich-cont">Выбрать саммит</span>' +
                '<span class="sandwich-button"></span><div class="sandwich-block"><ul>';
            for (var i = 0; i < data.length; i++) {
                html += '<li data-id="' + data[i].id + '"><span>' + data[i].title + '</span></li>';
            }
            html += '</ul></div></div>';
            document.getElementById(id).innerHTML =html;

        });
    
}

function update_summit(id,summit_id,money) {
      var data = {
                "user_id": id,
                "summit_id": summit_id,
                "value": money
            }
            console.log(data)
        var json = JSON.stringify(data);
        ajaxRequest(config.DOCUMENT_ROOT+'api/summit_ankets/post_anket/', json, function(JSONobj) {
            if(JSONobj.status){
                showPopup(JSONobj.message);
                summit_info(id);
                removePopups('register_summit_popup');
            } else {
                removePopups('register_summit_popup');
                showPopup(JSONobj.message);
            }
        }, 'POST', true, {
            'Content-Type': 'application/json'
        });
}

function summit_info(id){
    ajaxRequest(config.DOCUMENT_ROOT+'api/summit_ankets/?user=' + id, null, function(data) {
      var data = data.results;
      var html ='<table><tr><td>Название</td><td>Дата старта</td><td>Сумма</td></tr>';
            for(var i = 0; i < data.length; i++){
                  html +='<tr>'   
                            html += '<td data-id="'+data[i].summit_info.summit_id+'">' + data[i].summit_info.type +'</td>';                        
                            html += '<td>' + data[i].summit_info.start_date +'</td>';
                            html += '<td>' + data[i].value +'</td>'
                  html +='</tr>' 
            }            
            html += '</table>'
            document.getElementById('summit_table').innerHTML = html;
    }); 
    $("body").on('click', '#summit_table tr', function() {
        var id = $(this).find("td[data-id]").attr('data-id'),
            summit = $(this).find("td[data-id]").html();
            console.log(id);
            updateSummitValue(id,summit);
    })   
}




function updateSummitValue(id,summit){

    var sum_id = id;
    console.log(id);
    var el_id = 'update_summit_popup';
    var html = '<p>Изменение суммы на саммит'+summit+' : </p>'+
                '<p> Сума <input type="text" value = "" name= "money" id= "update_money" > </p>'+
                '<input type="button"  value = "Изменить" id= "change_summit" >';
    removePopups(el_id)
    createPopups(el_id, html, function() {});
    showPopupById(el_id);

    document.getElementById('change_summit').addEventListener('click',function(){
        var summit_id = sum_id;
        console.log(document.activeElement);
        var id = document.location.href.split('/')[document.location.href.split('/').length - 2],
            money = document.getElementById('update_money').value;
        update_summit(id,summit_id,money);
        removePopups(el_id);
    })
  }

})
