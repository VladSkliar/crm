// Отрисовка таблицы отчетов
function createUserInfoBySearch(data) {
    var html = '<table class="tab1 search" id="userinfo">';
    if (!data.results.length) {
        document.querySelector(".tab_content").innerHTML = '<span class="empty_list">нет подчиненныx пользвателей</span>';
        return;
    }
    var my_reports = data.results[0].my_reports;
    var titles = Object.keys(my_reports)
    html += '<tr>';

    for (var k = 0; k < titles.length; k++) {
        if (titles[k] == 'url' || titles[k] == 'id') continue
        html += '<th>' + titles[k] + '</th>';
    }
    html += '<th>Подчиненные</th><th>Анкеты</th></tr>'
    for (var i = 0; i < data.results.length; i++) {

        html += '<tr data-id="' + data.results[i]['id'] + '">';
        for (var prop in data.results[i].my_reports) {
            html += '<td>' + data.results[i].my_reports[prop]['value'] + '</td>'

        }
        html += '<td><a href="#" class="subordinate">подчиненные</a></td><td><a href="' + config.DOCUMENT_ROOT + '/account/' + data.results[i]['id'] + '" class="report_id">анкета</a></td>'
        html += '</tr>';
    }
    html += '</table>'

    document.querySelector(".tab_content").innerHTML = html;


    //Получение подчиненных
    [].forEach.call(document.querySelectorAll(".subordinate"), function(el) {
        el.addEventListener('click', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').attr('data-id');
            getUsersList(id)
        });
    });

}

function getUsersList(id) {

    var master_id = id || document.getElementsByClassName('admin_name')[0].getAttribute('data-id');
    var path = config.DOCUMENT_ROOT + 'api/users/?master=' + master_id;
    var data = {};

    [].forEach.call(document.querySelectorAll(".search_cont input"), function(el) {
        if (el.value) {
            data[el.getAttribute('name')] = el.value;
            el.value = '';
        }
    });

    ajaxRequest(path, data, function(answer) {
        createUserInfoBySearch(answer)
    })
}

//Поиск
$(".apply").click(function(e) {
    e.preventDefault();
    getUsersList();

});