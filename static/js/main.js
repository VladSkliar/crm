var config = {
//      'DOCUMENT_ROOT':'http://vocrm.org/',
    'DOCUMENT_ROOT': 'http://5.101.119.32:8008/',
    'pagination_count': 30, //Количество записей при пагинации
    'pagination_mini_count': 10
}


//Dropbox plugin jquery
$(".sandwich, .menu_item").click(function() {
    $(".sandwich").toggleClass("active");
});

$(document).on('click', '.arrow-table', function() {
    $(this).toggleClass('button_rotate');
});

$('.edit-close').click(function() {
    $(this).parent().remove();
});

$('.checkbox, .table-button span').click(function() {
    $(this).toggleClass('active');
});

$(document).on('click', '.sandwich-button', function() {
    $(this).toggleClass('button_rotate'); 
    $(this).siblings('.sandwich-block').toggle();
    $(this).parent().toggleClass('sandwich-open');
});

//Смена состояния любого Dropbox 
$(document).on('click', '.sandwich-block ul li span', function() {
    var level_;
    var tx = $(this).text();
    $(this).parents().siblings('.sandwich-cont').text(tx);
    $(this).parents().siblings('.sandwich-cont').attr('data-id', this.parentNode.getAttribute('data-id'))
    $(this).parents('.sandwich-block').toggle();
    $(this).parents().siblings('.sandwich-button').toggleClass('button_rotate');
    $(this).parents('.sandwich-wrap').toggleClass('shadow');

    var id = this.parentNode.getAttribute('data-id');
    var id_ = document.getElementsByClassName('sandwich-wrap')[0].getElementsByClassName('sandwich-cont')[0].getAttribute('data-id');
    if( document.getElementsByClassName('hierarchy-wrap')[0] ){
        var level = document.getElementsByClassName('hierarchy-wrap')[0].getElementsByClassName('sandwich-cont')[0].getAttribute('data-id');
    } 
    
    if (document.getElementsByClassName('hierarchy-wrap')[1]) {
        level_ = document.getElementsByClassName('hierarchy-wrap')[1].getElementsByClassName('sandwich-cont')[0].getAttribute('data-id');
    }

    //Генерация отвественного по 3 ранее выбранным dropbox
    if (id_ && level && level_ ) {
        createHierarhyDropBox_last(id_, level_);
    }

    //partners.html 

    if( document.getElementsByClassName('father')[0] ){
         var id = document.getElementsByClassName('sandwich-cont')[0].getAttribute('data-id');
        //console.log(id);
         getPartnersList(id);
         getDeals(id);
         

    }

    if( document.getElementsByClassName('summit')[0] ){
         var id = document.getElementsByClassName('sandwich-cont')[0].getAttribute('data-id');
        //console.log(id);
        show_summits(id);
            }

    
});

//Закрытие динамически созданных popups
live('click', '.popap_wrap .close' ,function(){
    this.parentNode.parentNode.style.display = 'none'
})

//settings_events.html
$(document).on('click', '.emplyee-middle .add-button', function() {
    $('.emplyee_pop').toggle();
    $(this).toggleClass('more');
});

$(document).on('click', '.event-main-button', function() {
    $(this).siblings('.add-edit-block').toggle();
    $(this).toggleClass('more');
});


$(function() {
    if (isElementExists(document.getElementsByClassName('news')[0])) {
        ajaxRequest(config.DOCUMENT_ROOT + 'api/users/current', null, function(data) {
            var user_id = data.id;
           // var user_id  = 1
            currentUser(data);

            //Подгрузка бокового меню на всех страницах
            ajaxRequest(config.DOCUMENT_ROOT + 'api/navigation/', null, function(data) {
                createMenu(data.results, 1);
            });

            if (isElementExists(document.getElementsByClassName('apply')[0])) {
                document.getElementsByClassName('apply')[0].click();
            }


            //settings.html смена данных пользователя
            if (isElementExists(document.querySelector('.personal-data'))) {

                document.getElementById('name').value = data.first_name;
                document.getElementById('surname').value = data.last_name;
                document.getElementById('pantronic').value = data.middle_name;
                document.querySelector('.personal-data .mail-input').value = data.email;
                document.getElementById('tel-input').value = data.phone_number;
            }

            //partners.html 
            
            if (isElementExists(document.getElementById('partner_wrap'))) {

               // var user_id = 1 // test  отвественный с таблици анкети
                    getPartnersList(user_id);
                    getDeals(user_id);
            }

            //settings_disaples

            if(document.getElementById('database_users')){
                
                createUser({'master':user_id}) ;

                document.getElementById('my_sub').addEventListener('click',function(){
                    document.getElementsByName('fullsearch')[0].value = ''
                     document.getElementById('dropbox_wrap').innerHTML =''
                            createDepartmentsDropBox(); //???
                            createHierarhyDropBox() //??
                            createUser({'master':user_id}) ;
        
                    })  
            }

        });

    }
    //Выпадающая панель поиска
    if (document.getElementsByClassName('topic_container')[0]) {
        document.getElementsByClassName('topic_container')[0].addEventListener('click', function() {

            var status = $('.content_block div.search_cont').css('display') == 'block' ? 'none' : 'block';
            $('.content_block div.search_cont').css('display', status);
        })
    }

});

//Выдвижное боковое меню
$(window).load(function() {
    if (isElementExists(document.getElementsByClassName('news')[0])) {

        $("[data-toggle]").click(function() {
            var toggle_el = $(this).data("toggle");
            $(toggle_el).toggleClass("open-sidebar");
        });
        $(".swipe-area").swipe({
            swipeStatus: function(event, phase, direction, distance, duration, fingers) {
                if (phase == "move" && direction == "right") {
                    $(".container").addClass("open-sidebar");
                    return false;
                }
                if (phase == "move" && direction == "left") {
                    $(".container").removeClass("open-sidebar");
                    return false;
                }
            }
        });
    }
});


//Календарь
jQuery(function($) {
    if($.datepicker){
            $.datepicker.regional['ru'] = {
            monthNames: ['Яварь', 'Февраль', 'Март', 'Апрель',
                'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
                'Октябрь', 'Ноябрь', 'Декабрь'
            ],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            firstDay: 1,
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    }
    
});
