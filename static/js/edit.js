	function init(id) {

	    var id = id || document.location.href.split('/')[document.location.href.split('/').length - 2]
	    ajaxRequest(config.DOCUMENT_ROOT + 'api/users/' + id, null, function(data) {
	        var profile = '';
	        var user_hierarchy = '';
	        var user_status = '';
	        var status_name = [];
	        for (var prop in data.fields) {
	            if (!data.fields.hasOwnProperty(prop)) continue

	            if (data.fields[prop]['type'] == 's') {
	                if (data.fields[prop].verbose == 'born_date') {
	                    profile += '<input type="text"  id="datepicker"  style="cursor:pointer;"  name="' +
	                        data.fields[prop].verbose + '" value="' + data.fields[prop]['value'] + '">';
	                } else {
	                    profile += '<li><p>' + prop + '*</p><input type="text" name="' + data.fields[prop].verbose + '" value="' + data.fields[prop]['value'] + '"></li>';
	                }
	            }
	            if (data.fields[prop]['type'] == 'h') {
	                user_hierarchy += '<li><p>' + prop + '*</p><span>' + data.fields[prop]['value'] + '</span></li>';
	            }
	            if (data.fields[prop]['type'] == 'b') {
	                var is_checked = data.fields[prop]['value'] ? 'checked' : '';
	                if (is_checked) {
	                    status_name.push(prop);
	                }
	            }
	            if (data.fields[prop]['type'] == 't') {
	                profile += '<li><p>' + prop + '*</p><textarea rows = "3" >' + data.fields[prop]['value'] + '</textarea>';
	            }
	        }

	      if(data.image){
			document.querySelector(".user-photo img").src= data.image
		}


	     var division_list  = data.division_fields;
        var list = Object.keys(division_list);
        var divisions = ''
        if(list.length){
            divisions = '<li>Привязка к департаментам:</li>'
        }
        else{
            divisions = '<li>У пользователя нету привязки к департаментам</li>'
        }
        for(var prop in division_list){
            divisions += '<li><span>' + prop + '</span></li>';
        }



	        getStatus(status_name);
	        document.getElementById('user-info').innerHTML = profile;
	        document.getElementById('status').innerHTML = user_hierarchy;
	       document.getElementById('status_division').innerHTML = divisions;
	        $("#datepicker").datepicker({
	            dateFormat: "yy-mm-dd"
	        });
	    });



	}

	//Получение иерархий пользователя

	function changeHierarhy(el) {
	    createHierarhyDropBox();
	    el.style.display = 'none'
	}


	//Винести в отдельную либу ,используется во многих местах и перейменовать 
	function createHierarhyDropBox() {

	    ajaxRequest(config.DOCUMENT_ROOT + 'api/departments/', null, function(data) {
	        var data = data.results;
	        var html = '<div class="sandwich-wrap department-wrap"><span class="sandwich-cont">Отдел</span>' +
	            '<span class="sandwich-button"></span><div class="sandwich-block"><ul>'
	        for (var i = 0; i < data.length; i++) {
	            html += '<li data-id="' + data[i].id + '" ><span>' + data[i].title + '</span></li>'
	        }
	        html += '</ul></div></div>';
	        document.getElementById('dropbox_wrap').innerHTML = html;
	        createHierarhyDropBox_low();
	        createHierarhyDropBox_low();
	    });

	}


	function createHierarhyDropBox_low() {

	    ajaxRequest(config.DOCUMENT_ROOT + 'api/hierarchy/', null, function(data) {
	        var data = data.results;
	        var html = '<div class="sandwich-wrap hierarchy-wrap"><span class="sandwich-cont">Ранг</span>' +
	            '<span class="sandwich-button"></span><div class="sandwich-block"><ul>';
	        for (var i = 0; i < data.length; i++) {
	            html += '<li data-id="' + data[i].id + '" data-level="' + data[i].level + '"><span>' + data[i].title + '</span></li>';
	        }
	        html += '</ul></div></div>';
	        document.getElementById('dropbox_wrap').innerHTML =
	            document.getElementById('dropbox_wrap').innerHTML + html;

	    });
	}

	/*
		function createHierarhyDropBox_last(id, level) {


		    ajaxRequest(config.DOCUMENT_ROOT+'api/users/?department=' + id + '&hierarchy=' + level, null, function(data) {
		        var data = data.results;
		        var html = '<div class="sandwich-wrap hierarchy__level"><span class="sandwich-cont">Отвественный</span>' +
		            '<span class="sandwich-button"></span><div class="sandwich-block"><ul>'
		        for (var i = 0; i < data.length; i++) {
		            html += '<li data-id="' + data[i].id + '" data-last="true"><span>' + data[i].fullname + '</span></li>'
		        }
		        html += '</ul></div></div>';
		        document.getElementById('dropbox_wrap').innerHTML =
		            document.getElementById('dropbox_wrap').innerHTML + html;

		    });

		}
	*/

	function createHierarhyDropBox_last(id, level, page) {
	    var page = page || 1;

	    ajaxRequest(config.DOCUMENT_ROOT + 'api/short_users/?department=' + id + '&hierarchy=' + level + '&page=' + page, null, function(data) {

	        var count = data.count; //Количество всех пользователей ?
	        var data = data.results;
	        var html = '';
	        var el_id = 'dep_1';
	        //paginations

	        html += '<p>Найдено ' + count + ' пользователей</p><p>Вибирите отвественного</p>';
	        html += '<ul class="lineTabs">';
	        var pages = Math.ceil(count / config.pagination_mini_count);

	        //if (pages > 1) {
	        for (var j = 1; j < pages + 1; j++) {
	            if (j == page) {
	                html += '<li><span class="page active">  ' + j + '</span></li>'
	            } else {
	                html += '<li><span class="page">  ' + j + '</span></li>'
	            }

	        }
	        // }

	        html += '</ul>';
	        for (var i = 0; i < data.length; i++) {
	            html += '<li data-id="' + data[i].id + '" class="item_user">' +
	                '<span class="checkbox" data-id ="' + data[i].id + '" ></span>' + data[i].fullname +
	                '</li>'
	        }


	        removePopups(el_id)
	        createPopups(el_id, html, function() {

	            [].forEach.call(document.getElementById(el_id).querySelectorAll("span.page"), function(el) {
	                el.addEventListener('click', function() {

	                    var page = el.innerHTML;
	                    createHierarhyDropBox_last(id, level, page)


	                });
	            });

	            [].forEach.call(document.querySelectorAll(".pop_cont span.checkbox"), function(tag) {



	                tag.addEventListener('click', function() {

	                    [].forEach.call(document.querySelectorAll(".pop_cont span.checkbox"), function(tag) {
	                        tag.classList.remove('active');
	                    })
	                    this.classList.toggle('active');

	                    document.getElementById('status').innerHTML = '<li>Ваш отвественный :' + this.nextSibling.textContent +
	                        '<input type="button"  value = "Сменить наставника"></li>';

	                    document.querySelector("#status input").addEventListener('click', function() {
	                        showPopupById(el_id);
	                    })


	                })


	            });

	            showPopupById(el_id);
	        });



	    });

	}


	function updateUser() {

	    var email = document.querySelector("input[name='email']").value;
	    var first_name = document.querySelector("input[name='first_name']").value;
	    var last_name = document.querySelector("input[name='last_name']").value;
	    var middle_name = document.querySelector("input[name='middle_name']").value;
	    var phone_number = document.querySelector("input[name='phone_number']").value;
	    var facebook = document.querySelector("input[name='facebook']").value;
	    var vkontakte = document.querySelector("input[name='vkontakte']").value;
	    var skype = document.querySelector("input[name='skype']").value;
	    var address = document.querySelector("input[name='address']").value;
	    var description = document.querySelector("textarea").value;
	    var date = document.querySelector("#datepicker").value;

	    var region = document.querySelector("input[name='region']").value;
	    var district = document.querySelector("input[name='district']").value;

	    //  var file = document.querySelector("input[name='super1']").value;

	    if ( /*!email ||*/ !first_name || !last_name /*|| !middle_name || !phone_number */ ) {
	        showPopup('Заполните пустые поля');
	        return
	    }

	    /*
	    	    if (!document.querySelectorAll(".user-status span.active").length) {
	    	        showPopup('Выберите статус');
	    	        return
	    	    }
	    */
	    var statuses = [];
	    [].forEach.call(document.querySelectorAll(".user-status span.active"), function(el) {
	        statuses.push(el.getAttribute('id'))
	    });


	    /*
	    if(  !isElementExists(document.querySelector(".sandwich-wrap.hierarchy__level .sandwich-cont"))  
	    	//&&  document.querySelector(".sandwich-wrap.hierarchy__level .sandwich-cont").getAttribute('data-id')
	    	){
	    	showPopup('Выбирите подчиненного');
	    	return
	    }

	    if(  !document.querySelector(".sandwich-wrap.hierarchy__level .sandwich-cont").getAttribute('data-id') ){
	    	showPopup('Выбирите подчиненного в последнем меню');
	    	return
	    }
	     */


	    var data = {
	        "id": document.location.href.split('/')[document.location.href.split('/').length - 2],
	        "email": email,
	        "first_name": first_name,
	        "last_name": last_name,
	        "middle_name": middle_name,
	        "born_date": date,
	        "phone_number": phone_number,
	        "vkontakte": vkontakte,
	        "facebook": facebook,
	        "address": address,
	        "description": description,
	        "skype": skype,
	        "statuses": statuses,
	        //  "image" : file,
	        "district": district,
	        "region": region
	    }

	    if (document.getElementById('dep_1') && document.querySelector("#dep_1 .checkbox.active")) {

	        data['master'] = document.querySelector("#dep_1 .checkbox.active").getAttribute('data-id');
	        data['department'] = document.querySelector(".sandwich-wrap.department-wrap .sandwich-cont").getAttribute('data-id');
	        data['hierarchy'] = document.querySelector(".sandwich-wrap.hierarchy-wrap .sandwich-cont").getAttribute('data-id');
	    }



	    if (document.getElementById('divisions_1') && document.querySelector("#divisions_1 .checkbox.active")) {
	        //document.querySelector("#divisions_1 .checkbox.active").getAttribute('data-id')

	        	var  arr = [];   
	        	$( "#divisions_1 .checkbox.active").each(function(){arr.push(this.getAttribute('data-id'))});
	        	data['divisions'] = arr
	    }



	    var json = JSON.stringify(data);


 ajaxRequest(config.DOCUMENT_ROOT + 'api/create_user/', json, function(data) {
	        showPopup(data.message);
	      //  console.log(data)
	        if (data.redirect) {

	        	//console.log(data.id)
	        	var fd = new FormData();    
				fd.append( 'file', $('input[type=file]')[0].files[0] );
				fd.append('id' , data.id)
				var xhr = new XMLHttpRequest();
				xhr.withCredentials = true;
				    xhr.open('POST',config.DOCUMENT_ROOT + 'api/create_user/', true);
				  //  xhr.setRequestHeader('Content-Type', 'application/json');
				    xhr.onload = function(answer){
			

				    	var answer = JSON.parse(answer.currentTarget.response)

				    	

				    	
				    //	showPopup(answer.message);
				       if (answer.redirect) {
					            setTimeout(function() {
					                window.location.href = '/account/' + answer.id;
					            }, 1000);
					        }
				    };
				    xhr.send(fd);
								




	
	        }
	    }, 'POST', true, {
	        'Content-Type': 'application/json'
	    });


	}

	//Получение статусов пользователя ..Левит	
	function getStatus(statuses) {

	    ajaxRequest(config.DOCUMENT_ROOT + 'api/statuses/', null, function(data) {
	        var data = data.results;
	        var html = '<ul>';
	        for (var i = 0; i < data.length; i++) {
	            html += '<li class="item_user clearfix"><i data-title="' + data[i].title + '">' +
	                data[i].title + '</i><span class="checkbox" id ="' + data[i].id + '" ></span></li>';
	        }
	        html += '</ul>';
	        document.getElementsByClassName('user-status')[0].innerHTML = html;
	        for (var j = 0; j < statuses.length; j++) {
	            $(" i[data-title='" + statuses[j] + "'] ").next().addClass('active');
	        }


	        [].forEach.call(document.querySelectorAll(".user-status span"), function(el) {
	            el.addEventListener('click', function() {
	                this.classList.toggle('active');
	            })

	        });

	    });
	}

function createDivisions() {
	    

	    ajaxRequest(config.DOCUMENT_ROOT + 'api/divisions/', null, function(data) {

	        
	        var data = data.results;
	        var html = '';
	        var el_id = 'divisions_1';
	      

	        for (var i = 0; i < data.length; i++) {
	            html += '<li data-id="' + data[i].id + '" class="item_user">' +
	                '<span class="checkbox" data-id ="' + data[i].id + '" ></span>' + data[i].title +
	                '</li>'
	        }


	        removePopups(el_id)
	        createPopups(el_id, html, function() {

	           

	            [].forEach.call(document.querySelectorAll("#divisions_1 span.checkbox"), function(tag) {



	                tag.addEventListener('click', function() {
	                	/*
	                    [].forEach.call(document.querySelectorAll("#divisions_1 span.checkbox"), function(tag) {
	                        tag.classList.remove('active');
	                    })
	*/
	                    this.classList.toggle('active');

	                })


	            });
	            showPopupById(el_id);
	        });


	    });

	}

	function handleFileSelect(evt) {
	
		    var files = evt.target.files; // FileList object

		    // Loop through the FileList and render image files as thumbnails.
		    for (var i = 0, f; f = files[i]; i++) {

		      // Only process image files.
		      if (!f.type.match('image.*')) {
		        continue;
		      }

		      var reader = new FileReader();

		      // Closure to capture the file information.
		      reader.onload = (function(theFile) {
		        return function(e) {
	
		          document.querySelector(".user-photo img").src= e.target.result
		        };
		      })(f);

		      // Read in the image file as a data URL.
		      reader.readAsDataURL(f);
		    }
  }

  
	$(function() {
	    init();
	    document.getElementsByName('f')[0].addEventListener('change', handleFileSelect, false);
	   document.getElementById('division').addEventListener('click',function(){

			   createDivisions()


	    })
	})