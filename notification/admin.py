from django.contrib import admin
from models import Notification


class NotificationAdmin(admin.ModelAdmin):
    list_display = ('theme', 'date', 'user', 'system', 'common', )

    class Meta:
        model = Notification

admin.site.register(Notification, NotificationAdmin)
