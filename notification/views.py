# -*- coding: utf-8
from models import Notification
from serializers import NotificationSerializer
from rest_framework.decorators import list_route
from rest_framework import viewsets, filters
from rest_framework.response import Response

from django.utils import timezone
from datetime import timedelta


class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @list_route()
    def today(self, request):
        weekday = timezone.now().weekday() + 1
        date = timezone.now().date()
        date_notifications = Notification.objects.filter(date=date).all()
        objects = date_notifications
        page = self.paginate_queryset(objects)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(objects, many=True)
        return Response(serializer.data)

    @list_route()
    def last_week(self, request):
        from_day = timezone.now().date()
        to_day = timezone.now().date() + timedelta(days=7)
        last_week_notifications = Notification.objects.filter(date__month__lte=from_day.month,
                                                              date__month__gte=to_day.month,
                                                              date__day__lte=from_day.day,
                                                              date__day__gte=to_day.day).all()
        system_notifications = Notification.objects.filter(system=True).all()
        objects = last_week_notifications | system_notifications
        page = self.paginate_queryset(objects)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(objects, many=True)
        return Response(serializer.data)
