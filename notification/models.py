from __future__ import unicode_literals
from django.db import models
from event.models import DayOfTheWeekField


class Notification(models.Model):
    user = models.ForeignKey('account.CustomUser', null=True, blank=True)
    theme = models.CharField(max_length=100)
    description = models.TextField()
    date = models.DateField(null=True, blank=True)
    day = DayOfTheWeekField(null=True, blank=True)
    common = models.BooleanField(default=True)
    system = models.BooleanField(default=False)

    class Meta:
        ordering = ['date']

