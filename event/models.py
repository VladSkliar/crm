# -*- coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext as _
from django.utils import timezone
from datetime import date
from django.db.models import signals
from django.dispatch import receiver
from collections import OrderedDict

DAY_OF_THE_WEEK = {
    '1': _(u'Monday'),
    '2': _(u'Tuesday'),
    '3': _(u'Wednesday'),
    '4': _(u'Thursday'),
    '5': _(u'Friday'),
    '6': _(u'Saturday'),
    '7': _(u'Sunday'),
}
weekday = timezone.now().weekday() + 1
default_date = timezone.now()

class DayOfTheWeekField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = tuple(sorted(DAY_OF_THE_WEEK.items()))
        kwargs['max_length'] = 1
        super(DayOfTheWeekField, self).__init__(*args, **kwargs)


ATTR_TYPES = (
    ('s', 'string'),
    ('b', 'boolean'),
    ('i', 'integer'),
)

VERBOSE_FIELDS = {'Имя': 'user__first_name',
                  'Фамилия': 'user__last_name',
                  'Отчество': 'user__middle_name',
                  'Иерархия': 'user__hierarchy',
                  'Отдел': 'user__department',
                  'Страна': 'user__country',
                  'Город': 'user__city',
                  'Примечание': 'description',
                  'Явка': 'check'}


class EventType(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to='images/eventTypes/', null=True, blank=True)

    def __unicode__(self):
        return self.title

    @property
    def event_count(self):
        return self.events.count()

    @property
    def last_event_date(self):
        last_event = self.events.last()
        return str(last_event.from_date)


class EventAnket(models.Model):
    user = models.OneToOneField('account.CustomUser', related_name='event_anket', )

    def __unicode__(self):
        return self.user.get_full_name()


class Event(models.Model):
    event_type = models.ForeignKey(EventType, related_name='events', blank=True, null=True)
    from_date = models.DateField(default=date.today())
    to_date = models.DateField(default=date.today())
    time = models.TimeField(default=timezone.now())
    users = models.ManyToManyField(EventAnket,
                                   through='Participation',
                                   through_fields=('event', 'user'),
                                   blank=True,
                                   related_name='events')

    class Meta:
        verbose_name_plural = "События"

    def __unicode__(self):
        return self.event_type.title


class Participation(models.Model):
    user = models.ForeignKey(EventAnket, related_name='participations')
    event = models.ForeignKey(Event, related_name='participations')
    check = models.BooleanField(default=False)
    value = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Участия"

    @property
    def check_count(self):
        return 0

    @property
    def uid(self):
        return self.user.user.id

    @property
    def hierarchy_chain(self):
        return self.user.user.hierarchy_chain

    @property
    def has_disciples(self):
        return self.user.user.has_disciples


    @property
    def fields(self):
        l = self.user.user.fields
        d = OrderedDict()
        d['value'] = self.check
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = u'check'
        l[u'Присутствие'] = d

        d = OrderedDict()
        d['value'] = self.value
        d['type'] = 'i'
        d['change'] = True
        d['verbose'] = u'value'
        l[u'Сумма'] = d

        d = OrderedDict()
        d['value'] = self.check_count
        d['type'] = 'i'
        d['change'] = True
        d['verbose'] = u'check_count'
        l[u'Пришло'] = d
        return l

@receiver(signals.post_save, sender=Event)
def sync_event(sender, instance, **kwargs):
    if not instance.participations.all():
        users = EventAnket.objects.all()
        for user in users:
            participation = Participation.objects.create(event=instance, user=user, value=0)


