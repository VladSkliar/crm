from django.contrib import admin
from models import Navigation, Table, ColumnType, Category, Column


class NavigationAdmin(admin.ModelAdmin):
    list_display = ('title',  'url', )
    #inlines = [ChoiceInline]

    class Meta:
        model = Navigation

admin.site.register(Navigation, NavigationAdmin)


class TableAdmin(admin.ModelAdmin):
    list_display = ('user', )
    #inlines = [ChoiceInline]

    class Meta:
        model = Table

admin.site.register(Table, TableAdmin)


class ColumnTypeTAdmin(admin.ModelAdmin):
    list_display = ('title', 'verbose_title', )
    #inlines = [ChoiceInline]

    class Meta:
        model = ColumnType

admin.site.register(ColumnType, ColumnTypeTAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'common', )
    #inlines = [ChoiceInline]

    class Meta:
        model = Category

admin.site.register(Category, CategoryAdmin)


class ColumnAdmin(admin.ModelAdmin):
    list_display = ('table', )
    #inlines = [ChoiceInline]

    class Meta:
        model = Column

admin.site.register(Column, ColumnAdmin)
