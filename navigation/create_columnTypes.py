from account.models import COMMON
from models import ColumnType


def create():
    for field in COMMON:
        columnType = ColumnType.objects.create(title=field)
        columnType.save()
        print "Created: " + columnType.title
    print "Done"
