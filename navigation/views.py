# -*- coding: utf-8
from models import Navigation, Table, ColumnType, Column
from rest_framework import viewsets, filters
from serializers import NavigationSerializer, TableSerializer, ColumnTypeSerializer, ColumnSerializer
from datetime import timedelta
from django.utils import timezone
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from collections import OrderedDict
from rest_framework.decorators import api_view
from django.shortcuts import render


class NavigationViewSet(viewsets.ModelViewSet):
    queryset = Navigation.objects.all()
    serializer_class = NavigationSerializer


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class ColumnTypeViewSet(viewsets.ModelViewSet):
    queryset = ColumnType.objects.all()
    serializer_class = ColumnTypeSerializer


class ColumnViewSet(viewsets.ModelViewSet):
    queryset = Column.objects.all()
    serializer_class = ColumnSerializer


@api_view(['POST'])
def update_columns(request):
    '''POST: (id, number, active)'''
    response_dict = dict()
    table = None
    if request.method == 'POST':
        for data in request.data:
            try:
                object = Column.objects.get(id=data['id'])
                table = object.table
                for key, value in data.iteritems():
                    if key == 'active' and not object.columnType.editable:
                        pass
                    else:
                        setattr(object, key, value)
                object.save()
                #serializer = ColumnSerializer(object, context={'request': request})
                #response_dict['data'] = serializer.data
            except Column.DoesNotExist:
                pass
    response_dict['message'] = u"Колонки успешно отредактирована"
    response_dict['status'] = True
    response_dict['column_table'] = table.user.column_table
    return Response(response_dict)
