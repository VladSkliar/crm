from django.contrib import admin
from models import SummitAnket, Summit, SummitType


admin.site.register(SummitAnket)
admin.site.register(Summit)
admin.site.register(SummitType)
