# -*- coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User, UserManager
from django.db.models.signals import post_save, post_init
from collections import OrderedDict
from report.models import UserReport
from event.models import Event
from django.utils import timezone
from datetime import timedelta
from tv_crm.models import LastCall
from event.models import EventAnket

VERBOSE_FIELDS = OrderedDict([('Имя', 'first_name'),
             ('Фамилия', 'last_name'),
             ('Отчество', 'middle_name'),
             ('Email', 'email'),
             ('Телефон', 'phone_number'),
             ('Дата рождения', 'born_date'),
             ('Иерархия', 'hierarchy'),
             ('Отдел', 'department'),
             ('Страна', 'country'),
             ('Область', 'region'),
             ('Населенный пункт', 'city'),
             ('Район', 'district'),
             ('Адрес', 'address'),
             ('Skype', 'skype'),
             ('Vkontakte', 'vkontakte'),
             ('Facebook', 'facebook')])

COMMON = ['Имя', 'Фамилия', 'Отчество','Email','Телефон', 'Дата рождения', 'Иерархия','Отдел',
          'Страна', 'Область', 'Населенный пункт', 'Район','Адрес', 'Skype', 'Vkontakte', 'Facebook', 'Отдел церкви',]


def get_hierarchy_chain(obj, l):
    d = OrderedDict()
    d['value'] = obj.get_full_name()
    d['id'] = obj.id
    l.append(d)
    master = obj.master
    if master:
        get_hierarchy_chain(master, l)


class CustomUser(User):
    middle_name = models.CharField(max_length=40, default='', blank=True)
    phone_number = models.CharField(max_length=13, default='', blank=True, null=True)
    skype = models.CharField(max_length=50, default='', null=True, blank=True)
    country = models.CharField(max_length=50, default='', blank=True)
    region = models.CharField(max_length=50, default='', blank=True)
    city = models.CharField(max_length=50, default='', blank=True)
    district = models.CharField(max_length=50, default='', blank=True)
    address = models.CharField(max_length=300, default='', null=True, blank=True)
    born_date = models.DateField(blank=True, null=True)
    facebook = models.URLField(default='', blank=True, null=True)
    vkontakte = models.URLField(default='', blank=True, null=True)
    image = models.ImageField(upload_to='images/', null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    department = models.ForeignKey('hierarchy.Department', related_name='users', null=True, blank=True, on_delete=models.SET_NULL)
    hierarchy = models.ForeignKey('hierarchy.Hierarchy', related_name='users', null=True, blank=True, on_delete=models.SET_NULL)
    master = models.ForeignKey('self', related_name='disciples', null=True, blank=True, on_delete=models.SET_NULL)
    objects = UserManager()

    def __unicode__(self):
        return self.username

    class Meta:
        ordering = ['id']

    @property
    def hierarchy_chain(self):
        l = list()
        get_hierarchy_chain(self, l)
        return l

    @property
    def has_disciples(self):
        if self.disciples.all():
            return True
        else:
            return False

    @property
    def column_table(self):
        l = OrderedDict()
        if self.table:
            columns = self.table.columns.order_by('number')
            for column in columns.all():
                d = OrderedDict()
                d['id'] = column.id
                d['title'] = column.columnType.verbose_title
                d['number'] = column.number
                d['active'] = column.active
                d['editable'] = column.editable
                l[column.columnType.title] = d
        return l

    @property
    def common(self):
        l = OrderedDict([('Имя', 'first_name'),
             ('Фамилия', 'last_name'),
             ('Отчество', 'middle_name'),
             ('Email', 'email'),
             ('Телефон', 'phone_number'),
             ('Дата рождения', 'born_date'),
             ('Иерархия', 'hierarchy'),
             ('Отдел', 'department'),
             ('Страна', 'country'),
             ('Область', 'region'),
             ('Населенный пункт', 'city'),
             ('Район', 'district'),
             ('Адрес', 'address'),
             ('Skype', 'skype'),
             ('Vkontakte', 'vkontakte'),
             ('Facebook', 'facebook'),
             ('Отдел церкви', 'divisions')])
        return l

    @property
    def my_reports(self):
        l = OrderedDict()
        d = OrderedDict()
        d['value'] = self.fullname
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'fullname'
        l[u'ФИО'] = d

        d = OrderedDict()
        d['value'] = self.hierarchy.title
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'hierarchy'
        l[u'Иерархия'] = d
        current_month = timezone.now().date().month
        reports = UserReport.objects.filter(user=self, date__month=current_month).all()
        for report in reports:

            title = report.event.title + '/' + str(report.date)
            d = OrderedDict()
            d['value'] = report.count
            d['type'] = 's'
            d['change'] = False
            d['verbose'] = 'count'
            l[title] = d
        return l

    @property
    def fields(self):
        l = OrderedDict()

        d = OrderedDict()
        d['value'] = self.fullname
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'fullname'
        l[u'ФИО'] = d

        d = OrderedDict()
        d['value'] = self.email
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'email'
        l[u'Email'] = d

        d = OrderedDict()
        if self.born_date:

            d['value'] = self.born_date
        else:
            d['value'] = ''
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'born_date'
        l[u'Дата рождения'] = d

        d = OrderedDict()
        d['value'] = self.phone_number
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'phone_number'
        l[u'Телефон'] = d

        d = OrderedDict()
        d['value'] = self.country
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'country'
        l[u'Страна'] = d

        d = OrderedDict()
        d['value'] = self.region
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'region'
        l[u'Область'] = d

        d = OrderedDict()
        d['value'] = self.city
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'city'
        l[u'Город'] = d

        d = OrderedDict()
        d['value'] = self.district
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'district'
        l[u'Район'] = d

        d = OrderedDict()
        d['value'] = self.address
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'address'
        l[u'Адрес'] = d

        d = OrderedDict()
        d['value'] = self.hierarchy.title
        d['type'] = 'h'
        d['change'] = False
        d['verbose'] = 'hierarchy'
        l[u'Иерархия'] = d

        d = OrderedDict()
        if self.department:
            d['value'] = self.department.title
        else:
            d['value'] = ''
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'department'
        l[u'Отдел'] = d

        d = OrderedDict()
        d['value'] = self.skype
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'skype'
        l[u'Skype'] = d

        d = OrderedDict()
        d['value'] = self.vkontakte
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'vkontakte'
        l[u'Вконтакте'] = d

        d = OrderedDict()
        d['value'] = self.facebook
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = 'facebook'
        l[u'Facebook'] = d

        d = OrderedDict()
        sl = list()
        if self.divisions:
            for division in self.divisions.all():
                sl.append(division.title)
            d['value'] = ','.join(sl)
        else:
            d['value'] = ''
        d['type'] = 't'
        d['change'] = False
        d['verbose'] = 'division'
        l[u'Отдел церкви'] = d


        d = OrderedDict()
        d['value'] = self.description
        d['type'] = 't'
        d['change'] = False
        d['verbose'] = 'description'
        l[u'Примечание'] = d
        return l

    @property
    def division_fields(self):
        l = OrderedDict()
        for division in self.divisions.all():
            d = OrderedDict()
            d['value'] = True
            d['type'] = 'b'
            d['change'] = False
            d['verbose'] = 'division.title'
            l[division.title] = d
        return l

    @property
    def fullname(self):
        return self.last_name + ' ' + self.first_name + ' ' + self.middle_name

    @property
    def hierarchy_name(self):
        return self.hierarchy.title

    @property
    def last_week_calls(self):
        day = timezone.now().date() - timedelta(days=7)
        count = LastCall.objects.filter(
            user__master=self,
            user__hierarchy__level=4,
            date__gte=day, date__lte=(timezone.now().date())).count()
        return count

    @property
    def attrs(self):
        l = [u'Ответственный', u'Отдел', u'Город', u'Номер телефона',
             u'Количество прозвонов за неделю', u'Посмотреть прозвоны']
        return l

    @property
    def department_title(self):
        l = self.department.title
        return l



def create_custom_user(sender, instance, created, **kwargs):
    if created:
        values = {}
        for field in sender._meta.local_fields:
            values[field.attname] = getattr(instance, field.attname)
        user = CustomUser(**values)
        user.save()

post_save.connect(create_custom_user, User)

from django.db.models import signals
from django.dispatch import receiver
from navigation.models import Table
from event.models import EventAnket


@receiver(signals.post_save, sender=CustomUser)
def sync_user(sender, instance, **kwargs):
    try:
        table = Table.objects.get(user=instance)
    except Table.DoesNotExist:
        table = Table.objects.create(user=instance)
    try:
        event_anket = EventAnket.objects.get(user=instance)
    except EventAnket.DoesNotExist:
        event_anket = EventAnket.objects.create(user=instance)

