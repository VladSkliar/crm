from account.models import CustomUser as User
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'email', 'fullname', 'image',
                  'hierarchy_name', 'has_disciples', 'column_table',
                  'fields', 'division_fields', 'hierarchy_chain', )


class UserShortSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'fullname')
