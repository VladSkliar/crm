# -*- coding: utf-8
from __future__ import unicode_literals
from collections import OrderedDict
from datetime import date
from django.db.models import Sum, Count

from django.db import models


class Partnership(models.Model):
    user = models.OneToOneField('account.CustomUser', related_name='partnership')
    responsible = models.ForeignKey('self', related_name='disciples', null=True, blank=True, on_delete=models.SET_NULL)
    value = models.IntegerField()
    date = models.DateField(default=date.today())
    is_responsible = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.get_full_name()

    @property
    def fullname(self):
        return self.user.get_full_name()

    @property
    def common(self):
        return [u'Пользователь', u'Ответственный', u'Сумма', u'Количество сделок', u'Итого']

    @property
    def result_value(self):
        if not self.is_responsible:
            deals = Deal.objects.filter(partnership=self).all().aggregate(sum_deals=Sum('value'))
        else:
            deals = Deal.objects.filter(partnership__responsible=self).all().aggregate(sum_deals=Sum('value'))
        if deals['sum_deals']:
            value = deals['sum_deals']
        else:
            value = 0
        return value

    @property
    def deals_count(self):
        if not self.is_responsible:
            value = self.deals.count()
        else:
            count = Partnership.objects.filter(responsible=self).all().annotate(num_authors=Count('deals'))
            value = len(count)
        return value


    @property
    def fields(self):
        l = self.user.fields
        d = OrderedDict()
        d['value'] = self.user.get_full_name()
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = u'user'
        l[u'Пользователь'] = d

        d = OrderedDict()
        if not self.is_responsible:
            d['value'] = self.responsible.user.get_full_name()
        else:
            d['value'] = ''
        d['type'] = 's'
        d['change'] = False
        d['verbose'] = u'responsible'
        l[u'Ответственный'] = d

        d = OrderedDict()
        d['value'] = self.value
        d['type'] = 'i'
        d['change'] = True
        d['verbose'] = u'value'
        l[u'Сумма сделки'] = d

        d = OrderedDict()
        d['value'] = self.deals_count
        d['type'] = 'i'
        d['change'] = False
        d['verbose'] = u'deals_count'
        l[u'Количество сделок'] = d

        if self.is_responsible:
            d = OrderedDict()
            d['value'] = self.disciples.count()
            d['type'] = 'i'
            d['change'] = False
            d['verbose'] = u'done_deals'
            l[u'Количество партнеров'] = d
        else:
            pass

        d = OrderedDict()
        d['value'] = self.result_value
        d['type'] = 'i'
        d['change'] = False
        d['verbose'] = u'result_value'
        l[u'Итого'] = d
        return l

    @property
    def deal_fields(self):
        l = OrderedDict()
        if self.is_responsible:
            deals = Deal.objects.filter(partnership__responsible=self).all()
        else:
            deals = Deal.objects.filter(partnership=self).all()
        l = list()
        for deal in deals:
            l.append(deal.fields)
        return l


class Deal(models.Model):
    date = models.DateField()
    value = models.IntegerField()
    partnership = models.ForeignKey('partnership.Partnership', related_name="deals")
    description = models.TextField()
    done = models.BooleanField(default=False)
    expired = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s : %s" % (self.partnership, self.date)

    @property
    def fields(self):
        l = OrderedDict()

        d = OrderedDict()
        d['value'] = self.id
        d['type'] = 'i'
        d['change'] = False
        d['verbose'] = u'id'
        l[u'Идентификатор сделки'] = d

        d = OrderedDict()
        d['value'] = self.partnership.user.get_full_name()
        d['type'] = 's'
        d['change'] = True
        d['verbose'] = u'user'
        l[u'Клиент'] = d

        d = OrderedDict()
        if self.partnership.responsible.user:
            d['value'] = self.partnership.responsible.user.get_full_name()
        else:
            d['value'] = ""
        d['type'] = 's'
        d['change'] = True
        d['verbose'] = u'responsible'
        l[u'Ответственный'] = d

        d = OrderedDict()
        d['value'] = self.date
        d['type'] = 's'
        d['change'] = True
        d['verbose'] = u'date'
        l[u'Дата'] = d

        d = OrderedDict()
        d['value'] = self.value
        d['type'] = 'i'
        d['change'] = True
        d['verbose'] = u'value'
        l[u'Сумма'] = d

        d = OrderedDict()
        d['value'] = self.description
        d['type'] = 's'
        d['change'] = True
        d['verbose'] = u'value'
        l[u'Описание'] = d


        d = OrderedDict()
        if self.done:
            d['value'] = u"Завершена"
        else:
            if self.expired:
                d['value'] = u"Просрочена"
            else:
                d['value'] = u"Не завершена"
        d['type'] = 'b'
        d['change'] = True
        d['verbose'] = u'done'
        l[u'Статус'] = d

        return l



