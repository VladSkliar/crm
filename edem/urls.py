from django.contrib import admin
from django.conf.urls import url, include
from rest_framework import routers
import account.views
import hierarchy.views
import notification.views
import event.views
import report.views
import status.views
import navigation.views
import partnership.views
import tv_crm.views
import summit.views


router = routers.DefaultRouter()
router.register(r'users', account.views.UserViewSet)
router.register(r'short_users', account.views.UserShortViewSet)
router.register(r'hierarchy', hierarchy.views.HierarchyViewSet)
router.register(r'departments', hierarchy.views.DepartmentViewSet)
router.register(r'notifications', notification.views.NotificationViewSet)
router.register(r'event_types', event.views.EventTypeViewSet)
router.register(r'event_ankets', event.views.EventAnketViewSet)
router.register(r'events', event.views.EventViewSet)
router.register(r'participations', event.views.ParticipationViewSet)
router.register(r'user_reports', report.views.UserReportViewSet)
router.register(r'day_reports', report.views.DayReportViewSet)
router.register(r'month_reports', report.views.MonthReportViewSet)
router.register(r'year_reports', report.views.YearReportViewSet)
router.register(r'statuses', status.views.StatusViewSet)
router.register(r'divisions', status.views.DivisionViewSet)
router.register(r'navigation', navigation.views.NavigationViewSet)
router.register(r'partnerships', partnership.views.PartnershipViewSet)
router.register(r'deals', partnership.views.DealViewSet)
router.register(r'tv_call', tv_crm.views.LastCallViewSet)
router.register(r'tv_call_stat', tv_crm.views.CallStatViewSet)
router.register(r'synopsis', tv_crm.views.SynopsisViewSet)
router.register(r'summit', summit.views.SummitViewSet)
router.register(r'summit_ankets', summit.views.SummitAnketViewSet)
router.register(r'tables', navigation.views.TableViewSet)
router.register(r'columnTypes', navigation.views.ColumnTypeViewSet)
router.register(r'columns', navigation.views.ColumnViewSet)
router.register(r'summit_types', summit.views.SummitTypeViewSet)
router.register(r'summit_search', summit.views.SummitUnregisterUserViewSet)


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('account.urls')),
    url(r'^api/', include('event.urls')),
    url(r'^api/', include('partnership.urls')),
    url(r'^api/', include('tv_crm.urls')),
    url(r'^api/', include('hierarchy.urls')),
    url(r'^api/', include('navigation.urls')),
    url(r'^', include('main.urls')),
    url(r'^', include('django.contrib.auth.urls')),
]
